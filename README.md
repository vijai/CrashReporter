# CrashReporter
| Min Api   |   CI Build    |      Download      |  License  |
|:----------:|:-------------|:-------------:|:------:|
| [![API](https://img.shields.io/badge/API-15%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=15) |  [![pipeline status](https://gitlab.com/vijai/CrashReporter/badges/master/pipeline.svg)](https://gitlab.com/vijai/CrashReporter/commits/master)  |  [![Bintray](https://img.shields.io/bintray/v/vijaichander/maven/Crash-Reporter?label=Download)](https://bintray.com/vijaichander/maven/Crash-Reporter/_latestVersion) | [![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) |

## Why CrashReporter? 
 CrashReporter is a handy library to capture app crashes and catched exceptions to device without logging any user sensitive data or donating data to 3rd party crash analytics. It is a light-weight library that stores the crashes and exceptions to file locally which can be shared or uploaded to Dogbin to share it with others. This library does not require root.


## Example
![In Action](assets/crash_reporter_work_flow.gif?raw=true "Gif")

# Crash Reporter APIs

- Track all crashes
- Use Log Exception API to log Exception
- All crashes and exceptions are saved in device
- Share logs with developers or orthers easily via share intent or by uploading [Dogbin](https://del.dog)
- ~~Choose your own path to save crash reports and exceptions~~ **Deprecated**

## Using Crash Reporter Library in your application
add below dependency in your app's gradle
```
implementation 'com.orpheusdroid.crashreporter:crashreporter:1.0.4'
```
## If you only want to use Crash reporter in debug builds only add
```
debugImplementation 'com.orpheusdroid.crashreporter:crashreporter:1.0.1'
releaseImplementation 'com.orpheusdroid.crashreporter:crashreporter-no-op:1.0.1'
```
**Note:** The no-op implementation does not have any functionality but implements essential empty classes to prevent the release build from failing to find the required classes.

# Wiki
CrashReporter captures all logs and saves to
```
/Android/data/your-app-package-name/files/crashReports
```

## Saving to custom location (Deprecated, will be removed in next version)
To save crashes in a path of your choice, require WRITE_EXTERNAL_STORAGE permission in manifest.
Make sure to request permission to user if the target android version is above marshmallow. The library does not request it for you.

```
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```
add below line in onCreate method of your Application class
```
CrashReporter.initialize(this, crashReporterPath);
```
Note: You don't need to call CrashReporter.initialize() if you want logs to be saved in default directory. Make sure to call this only if storage permission is granted.

Library would revert to saving the logs to default directory if the selected custom directory is not writable or no permission is granted.

## Enabling log upload to Dogbin
If you would like to enable the option of uploading logs to [Dogbin](https://del.dog) manually:

 - Add `<uses-permission android:name="android.permission.INTERNET" />` to the application manifest
 - Create `res/xml/network_security_config.xml` with the below config:
 ```
<?xml version="1.0" encoding="utf-8"?>  
<network-security-config>  
    <domain-config>
        <domain includeSubdomains="true">del.dog</domain>  
    </domain-config>
</network-security-config>
```
 - Add `android:networkSecurityConfig="@xml/network_security_config"` to application tag in application's manifest.

	 
## Logging caught exception
```
try {
    // Do your stuff
} catch (Exception e) {
    CrashReporter.logException(e);
}
```

## Viewing crash logs
This library comes with a simple layout to view all uncaught crashes and caught exceptions and it can be viewed using `CrashReporterActivity`

```
Intent intent = new Intent(mContext, CrashReporterActivity.class);
startActivity(intent);
```

If you are using no-op version, use
```
if (BuildConfig.DEBUG) {  
    Intent intent = new Intent(mContext, CrashReporterActivity.class);  
  startActivity(intent);  
}
```

Failing to do so might cause `ActivityNotFoundException`

## Proguard Config
If you are using proguard minify and also use the upload feature, add the below line to your proguard config:
```
-keep public class com.orpheusdroid.crashreporter.networking.model.** {*;}
```

## To get default crash reports path
```
CrashUtil.getDefaultPath()
```
you can access all crash/exception log files from this path and upload them to server for your need. Remember it's default path 
if you provide your own path you know where to find the logs...

# Find this project useful ? :heart:
* Support it by clicking the :star: button on the upper right of this page. :v:


# Fork Notice:
This project was forked from [https://github.com/MindorksOpenSource/CrashReporter](https://github.com/MindorksOpenSource/CrashReporter) authored by [MindorksOpenSource](https://github.com/MindorksOpenSource)

This library has following changes:

 - [x] Uses APP's native theme colors
 - [x] Fixed string resources
 - [x] Remove personally identifiable information
 - [x] Upload log to [Dogbin](https://del.dog)

# Original Project

[https://github.com/MindorksOpenSource/CrashReporter](https://github.com/MindorksOpenSource/CrashReporter)

# License

   ```
   Copyright (C) 2016 Bal Sikandar
   Copyright (C) 2020 Vijai Chander

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   ```
   
### Contributing to this Repo
   Create a pull request and Dive In.
