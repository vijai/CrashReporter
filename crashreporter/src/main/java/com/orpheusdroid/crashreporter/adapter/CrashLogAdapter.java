package com.orpheusdroid.crashreporter.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.orpheusdroid.crashreporter.R;
import com.orpheusdroid.crashreporter.ui.LogMessageActivity;
import com.orpheusdroid.crashreporter.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by bali on 10/08/17.
 */

public class CrashLogAdapter extends RecyclerView.Adapter<CrashLogAdapter.CrashLogViewHolder> {

    private Context context;
    private ArrayList<File> crashFileList;

    public CrashLogAdapter(Context context, ArrayList<File> allCrashLogs) {
        this.context = context;
        crashFileList = allCrashLogs;
    }

    @NonNull
    @Override
    public CrashLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_item, parent, false);
        return new CrashLogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CrashLogViewHolder holder, int position) {
        holder.setUpViewHolder(context, crashFileList.get(position));
    }

    @Override
    public int getItemCount() {
        return crashFileList.size();
    }


    public void updateList(ArrayList<File> allCrashLogs) {
        crashFileList = allCrashLogs;
        notifyDataSetChanged();
    }


    static class CrashLogViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewMsg, messageLogTime;
        private LinearLayout parentLayout;

        CrashLogViewHolder(View itemView) {
            super(itemView);
            messageLogTime = itemView.findViewById(R.id.messageLogTime);
            textViewMsg = itemView.findViewById(R.id.textViewMsg);
            parentLayout = itemView.findViewById(R.id.crLogsParentLayout);
        }

        void setUpViewHolder(final Context context, final File file) {
            final String filePath = file.getAbsolutePath();
            String date = file.getName().replaceAll("[a-zA-Z_.]", "");
            date = date.split(" ")[0] + " " + date.split(" ")[1].replaceAll("[-]", ":");
            messageLogTime.setText(date);
            textViewMsg.setText(FileUtils.readFirstLineFromFile(new File(filePath)));

            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LogMessageActivity.class);
                    intent.putExtra("LogMessage", filePath);
                    context.startActivity(intent);
                }
            });
        }
    }
}
