package com.orpheusdroid.crashreporter.networking;

import com.orpheusdroid.crashreporter.networking.interfaces.ApiService;
import com.orpheusdroid.crashreporter.networking.model.DogbinResponse;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Callback;

public class LogUploader {
    public static final String BASE_URL = "https://del.dog/";

    public static void uploadLog(String log, String deviceDetails, Callback<DogbinResponse> callback) {
        String data = log +
                "\n\n" +
                "################ DEVICE INFO ###############" +
                "\n\n" +
                deviceDetails;
        String result = "";

        ApiService apiService = Client.getClient().create(ApiService.class);
        RequestBody body = RequestBody.create(MediaType.parse(data), data);

        apiService.uploadLog(body).enqueue(callback);
    }

}
