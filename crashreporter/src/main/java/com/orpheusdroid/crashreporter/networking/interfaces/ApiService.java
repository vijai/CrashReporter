package com.orpheusdroid.crashreporter.networking.interfaces;

import com.orpheusdroid.crashreporter.networking.model.DogbinResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiService {
    @POST("/documents")
    Call<DogbinResponse> uploadLog(@Body RequestBody data);
}
