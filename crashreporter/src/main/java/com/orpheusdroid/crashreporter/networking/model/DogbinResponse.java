package com.orpheusdroid.crashreporter.networking.model;

import androidx.annotation.NonNull;

public class DogbinResponse {
    private String isUrl;
    private String key;

    public String getIsUrl() {
        return isUrl;
    }

    public void setIsUrl(String isUrl) {
        this.isUrl = isUrl;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @NonNull
    @Override
    public String toString() {
        return "DogbinResponse{" +
                "isUrl='" + isUrl + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
