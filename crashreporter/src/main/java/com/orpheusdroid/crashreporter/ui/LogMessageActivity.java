package com.orpheusdroid.crashreporter.ui;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.orpheusdroid.crashreporter.R;
import com.orpheusdroid.crashreporter.networking.LogUploader;
import com.orpheusdroid.crashreporter.networking.model.DogbinResponse;
import com.orpheusdroid.crashreporter.utils.AppUtils;
import com.orpheusdroid.crashreporter.utils.CrashUtil;
import com.orpheusdroid.crashreporter.utils.FileUtils;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogMessageActivity extends AppCompatActivity implements Callback<DogbinResponse> {

    private TextView appInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_message);
        appInfo = findViewById(R.id.appInfo);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("LogMessage")) {
                String dirPath = intent.getStringExtra("LogMessage");
                assert dirPath != null;
                initViews(dirPath);
            }
            if (intent.getAction() != null && intent.getAction().equals("com.orpheusdroid.crashreporter.OPEN_LOG_FROM_NOTIFICATION")) {
                String dirPath = PreferenceManager.getDefaultSharedPreferences(this)
                        .getString("OPEN_LOG_FROM_NOTIFICATION", "");
                initViews(dirPath);
            }
        }

        Toolbar myToolbar = findViewById(R.id.toolbar);
        myToolbar.setTitle(getString(R.string.crash_reporter));
        setSupportActionBar(myToolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getAppInfo();
    }

    private void initViews(String dirPath) {
        File file = new File(dirPath);
        String crashLog = FileUtils.readFromFile(file);
        TextView textView = findViewById(R.id.logMessage);
        textView.setText(crashLog);
    }

    private void getAppInfo() {
        appInfo.setText(AppUtils.getDeviceDetails(this));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        int res = checkCallingOrSelfPermission(Manifest.permission.INTERNET);

        if (res != PackageManager.PERMISSION_GRANTED) {
            menu.removeItem(menu.findItem(R.id.menu_upload_log).getItemId());
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.crash_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent = getIntent();
        String filePath = null;
        if (intent != null) {
            filePath = intent.getStringExtra("LogMessage");
        }

        if (filePath == null) {
            Toast.makeText(this, "Log Not Found", Toast.LENGTH_SHORT).show();
            return true;
        }

        if (item.getItemId() == R.id.delete_log) {
            showAlertAndDelete(filePath);
            return true;
        } else if (item.getItemId() == R.id.share_crash_log) {
            CrashUtil.shareCrashReport(this, filePath, appInfo.getText().toString());
            return true;
        } else if (item.getItemId() == R.id.menu_upload_log) {
            LogUploader.uploadLog(FileUtils.readFromFile(new File(filePath)), appInfo.getText().toString(), this);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void showAlertAndDelete(final String filePath) {
        MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(this)
                .setMessage(R.string.cr_alert_delete_log_title)
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton(R.string.cr_alert_delete_log_positive_btn_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (FileUtils.delete(filePath)) {
                            finish();
                        }
                    }
                })
                .setCancelable(true);
        dialogBuilder.create().show();
    }


    @Override
    public void onResponse(@NonNull Call<DogbinResponse> call, Response<DogbinResponse> response) {
        if (response.isSuccessful()) {
            DogbinResponse responseBody = response.body();

            if (responseBody == null) {
                Toast.makeText(this, R.string.toast_log_upload_invalid_response_message, Toast.LENGTH_SHORT).show();
                return;
            }

            final String url = LogUploader.BASE_URL + responseBody.getKey();
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("log", url);
            if (clipboard != null)
                clipboard.setPrimaryClip(clip);

            Snackbar.make(appInfo, R.string.toast_log_upload_success_message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.menu_share, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CrashUtil.shareLogURL(LogMessageActivity.this, url);
                        }
                    }).show();
        } else {
            Toast.makeText(this, getString(R.string.toast_log_upload_failure_message) + response.code(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(@NonNull Call<DogbinResponse> call, @NonNull Throwable t) {
        Toast.makeText(this, R.string.toast_log_upload_error_message, Toast.LENGTH_SHORT).show();
    }
}
