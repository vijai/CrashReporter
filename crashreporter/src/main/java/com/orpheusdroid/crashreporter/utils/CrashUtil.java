package com.orpheusdroid.crashreporter.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import com.orpheusdroid.crashreporter.CrashReporter;
import com.orpheusdroid.crashreporter.R;
import com.orpheusdroid.crashreporter.reporter.FilesProvider;
import com.orpheusdroid.crashreporter.ui.LogMessageActivity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.orpheusdroid.crashreporter.utils.Constants.CHANNEL_NOTIFICATION_ID;

public class CrashUtil {

    private static final String TAG = CrashUtil.class.getSimpleName();

    private CrashUtil() {
        //this class is not publicly instantiable
    }

    private static String getCrashLogTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    static void saveCrashReport(final Throwable throwable) {

        String crashReportPath = CrashReporter.getCrashReportPath();
        String filename = getCrashLogTime() + Constants.CRASH_SUFFIX + Constants.FILE_EXTENSION;
        String filePath = writeToFile(crashReportPath, filename, getStackTrace(throwable));

        showNotification(throwable.getLocalizedMessage(), filePath);
    }

    public static void logException(final Exception exception) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                String crashReportPath = CrashReporter.getCrashReportPath();
                final String filename = getCrashLogTime() + Constants.EXCEPTION_SUFFIX + Constants.FILE_EXTENSION;
                String filePath = writeToFile(crashReportPath, filename, getStackTrace(exception));

                showNotification(exception.getLocalizedMessage(), filePath);
            }
        }).start();
    }

    private static String writeToFile(String crashReportPath, String filename, String crashLog) {

        if (TextUtils.isEmpty(crashReportPath)) {
            crashReportPath = getDefaultPath();
        }

        File crashDir = new File(crashReportPath);
        String filePath;
        if (!crashDir.exists() || !crashDir.isDirectory() || !crashDir.canWrite()) {
            crashReportPath = getDefaultPath();
            Log.e(TAG, "Path provided doesn't exists or not writable : " + crashDir + "\nSaving crash report at : " + getDefaultPath());
        }

        BufferedWriter bufferedWriter;
        try {
            filePath = crashReportPath + File.separator + filename;
            bufferedWriter = new BufferedWriter(new FileWriter(filePath));

            bufferedWriter.write(crashLog);
            bufferedWriter.flush();
            bufferedWriter.close();
            Log.d(TAG, "crash report saved in : " + crashReportPath);
            return filePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static void showNotification(String localisedMsg, String filePath) {

        if (CrashReporter.isNotificationEnabled()) {
            Context context = CrashReporter.getContext();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            prefs.edit().putString("OPEN_LOG_FROM_NOTIFICATION", filePath)
                    .apply();

            NotificationManager notificationManager = (NotificationManager) context.
                    getSystemService(NOTIFICATION_SERVICE);

            createNotificationChannel(notificationManager, context);

            // Notification click action Intent
            Intent intent = new Intent(context, LogMessageActivity.class);
            intent.setAction("com.orpheusdroid.crashreporter.OPEN_LOG_FROM_NOTIFICATION");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            // Notification share file action intent
            Intent shareFileIntent = new Intent(context, NotificationBroadcastReceiver.class)
                    .setAction("com.orpheusdroid.crashreporter.SHARE_LOG");
            PendingIntent shareFilePendingIntent = PendingIntent.getBroadcast(context, 1, shareFileIntent, 0);

            // Notification share Url action intent
            Intent shareUrlIntent = new Intent(context, NotificationBroadcastReceiver.class)
                    .setAction("com.orpheusdroid.crashreporter.SHARE_LINK");
            PendingIntent shareUrlPendingIntent = PendingIntent.getBroadcast(context, 2, shareUrlIntent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_NOTIFICATION_ID);
            builder.setSmallIcon(R.drawable.ic_warning_black_24dp)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentTitle(context.getString(R.string.view_crash_report))
                    .setColor(ContextCompat.getColor(context, R.color.colorAccent))
                    .addAction(0, "Share File", shareFilePendingIntent)
                    .addAction(0, "Upload and share URL", shareUrlPendingIntent)
                    .setContentIntent(pendingIntent);

            if (TextUtils.isEmpty(localisedMsg)) {
                builder.setContentText(context.getString(R.string.check_your_message_here));
            } else {
                builder.setContentText(localisedMsg);
            }

            if (notificationManager != null) {
                notificationManager.notify(Constants.NOTIFICATION_ID, builder.build());
            }
        }
    }

    private static void createNotificationChannel(NotificationManager notificationManager, Context context) {
        if (Build.VERSION.SDK_INT >= 26) {
            CharSequence name = context.getString(R.string.notification_crash_report_title);
            String description = "";
            NotificationChannel channel = new NotificationChannel(CHANNEL_NOTIFICATION_ID, name, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(description);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private static String getStackTrace(Throwable e) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);

        e.printStackTrace(printWriter);
        String crashLog = result.toString();
        printWriter.close();
        return crashLog;
    }

    public static String getDefaultPath() {
        String defaultPath = CrashReporter.getContext().getExternalFilesDir(null).getAbsolutePath()
                + File.separator + Constants.CRASH_REPORT_DIR;

        File file = new File(defaultPath);
        file.mkdirs();
        return defaultPath;
    }

    public static void shareCrashReport(Context context, String filePath, String content) {
        Uri uri = FilesProvider.getUriForFile(context.getApplicationContext(), context.getPackageName() + ".com.orpheusdroid.crashreporter.provider.fileprovider", new File(filePath));
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, content);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        Intent chooserIntent = Intent.createChooser(intent, context.getString(R.string.intent_share_title));
        chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(chooserIntent);
    }

    public static void shareLogURL(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, url);

        Intent viewIntent = new Intent(Intent.ACTION_VIEW);
        viewIntent.setData(Uri.parse(url));

        Intent chooserIntent = Intent.createChooser(intent, context.getString(R.string.intent_share_title));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{viewIntent});
        chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(chooserIntent);
    }
}
