package com.orpheusdroid.crashreporter.utils;

/**
 * Created by bali on 15/08/17.
 */

public class Constants {
    public static final String EXCEPTION_SUFFIX = "_exception";
    public static final String CRASH_SUFFIX = "_crash";
    static final String FILE_EXTENSION = ".txt";
    static final String CRASH_REPORT_DIR = "crashReports";
    static final int NOTIFICATION_ID = 1;
    static final String CHANNEL_NOTIFICATION_ID = "crashreporter_channel_id";
    public static final String LANDING = "landing";
}
