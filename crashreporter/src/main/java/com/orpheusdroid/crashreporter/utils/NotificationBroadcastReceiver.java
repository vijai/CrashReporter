package com.orpheusdroid.crashreporter.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import com.orpheusdroid.crashreporter.R;
import com.orpheusdroid.crashreporter.networking.LogUploader;
import com.orpheusdroid.crashreporter.networking.model.DogbinResponse;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationBroadcastReceiver extends BroadcastReceiver implements Callback<DogbinResponse> {
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.mContext = context;
        String action = intent.getAction();
        String fileName = PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString("OPEN_LOG_FROM_NOTIFICATION", "");

        if (action != null && fileName.equals("")) {

            switch (action) {
                case "com.orpheusdroid.crashreporter.SHARE_LOG":
                    CrashUtil.shareCrashReport(context, fileName, AppUtils.getDeviceDetails(context));
                    break;
                case "com.orpheusdroid.crashreporter.SHARE_LINK":
                    LogUploader.uploadLog(FileUtils.readFromFile(new File(fileName)), AppUtils.getDeviceDetails(context), this);
                    break;
            }
        }
    }

    @Override
    public void onResponse(@NonNull Call<DogbinResponse> call, @NonNull Response<DogbinResponse> response) {
        if (response.isSuccessful()) {
            DogbinResponse response1 = response.body();
            String url;
            if (response1 != null) {
                url = LogUploader.BASE_URL + response1.getKey();
                CrashUtil.shareLogURL(mContext, url);
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<DogbinResponse> call, @NonNull Throwable t) {
        Toast.makeText(mContext, R.string.toast_log_upload_error_message, Toast.LENGTH_SHORT).show();
    }
}
