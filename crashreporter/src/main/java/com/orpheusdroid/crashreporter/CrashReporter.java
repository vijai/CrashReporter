package com.orpheusdroid.crashreporter;

import android.content.Context;
import android.content.Intent;

import com.orpheusdroid.crashreporter.exception.CrashReporterNotInitializedException;
import com.orpheusdroid.crashreporter.exception.NotEnoughPermissionException;
import com.orpheusdroid.crashreporter.ui.CrashReporterActivity;
import com.orpheusdroid.crashreporter.utils.CrashReporterExceptionHandler;
import com.orpheusdroid.crashreporter.utils.CrashUtil;

import java.io.File;

public class CrashReporter {

    private static Context applicationContext;

    private static String crashReportPath;

    private static boolean isNotificationEnabled = true;

    private CrashReporter() {
        // This class in not publicly instantiable
    }

    public static void initialize(Context context) {
        applicationContext = context;
        setUpExceptionHandler();
    }

    /**
     * @deprecated Will be removed when the library targets Android 11 due to scoped storage changes.
     */
    public static void initialize(Context context, String crashReportSavePath) {
        applicationContext = context;
        crashReportPath = crashReportSavePath;
        File path = new File(crashReportPath);
        if (!path.exists() || !path.isDirectory()) {
            if (!path.mkdirs()) {
                throw new NotEnoughPermissionException("Cannot create directory. Make sure the app can write to the directory.", NotEnoughPermissionException.ErrorCode.FOLDER_CREATE_ERROR);
            }
        }
        if (!path.canWrite())
            throw new NotEnoughPermissionException("Cannot write to directory. Make sure the app can write to the directory.", NotEnoughPermissionException.ErrorCode.FOLDER_NOT_WRITABLE);
        setUpExceptionHandler();
    }

    private static void setUpExceptionHandler() {
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CrashReporterExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CrashReporterExceptionHandler());
        }
    }

    public static Context getContext() {
        if (applicationContext == null) {
            try {
                throw new CrashReporterNotInitializedException("Initialize CrashReporter : call CrashReporter.initialize(context, crashReportPath)");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return applicationContext;
    }

    public static String getCrashReportPath() {
        return crashReportPath;
    }

    public static boolean isNotificationEnabled() {
        return isNotificationEnabled;
    }

    //LOG Exception APIs
    public static void logException(Exception exception) {
        CrashUtil.logException(exception);
    }

    public static Intent getLaunchIntent() {
        return new Intent(applicationContext, CrashReporterActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    public static void disableNotification() {
        isNotificationEnabled = false;
    }

}
