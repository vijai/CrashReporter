package com.orpheusdroid.crashreporter.exception;

public class NotEnoughPermissionException extends CrashReporterException {
    private static final long serialVersionUID = 7718828512143293558L;

    private final ErrorCode code;

    public NotEnoughPermissionException(ErrorCode code) {
        super();
        this.code = code;
    }

    public NotEnoughPermissionException(String message, Throwable cause, ErrorCode code) {
        super(message, cause);
        this.code = code;
    }

    public NotEnoughPermissionException(String message, ErrorCode code) {
        super(message);
        this.code = code;
    }

    public NotEnoughPermissionException(Throwable cause, ErrorCode code) {
        super(cause);
        this.code = code;
    }

    public ErrorCode getCode() {
        return this.code;
    }

    public enum ErrorCode {
        FOLDER_CREATE_ERROR(100),
        FOLDER_NOT_WRITABLE(101);

        public int errorCode;

        ErrorCode(int httpCode) {
            this.errorCode = errorCode;
        }

        public int getErrorCode() {
            return errorCode;
        }
    }
}
