package com.orpheusdroid.crashreporter.reporter;

import androidx.core.content.FileProvider;

public class FilesProvider extends FileProvider {
    // Empty subclass of FileProvider to prevent conflict with the implementing application
}
